package jp.kronos.dto;

import java.sql.Timestamp;

/**
 * ã¦ã¼ã¶æ?å ±
 * @author Mr.X
 */
public class UserDto {
	
	/* ã¦ã¼ã¶ID */
	private int userId;
	
	/* eã¡ã¼ã« */
	private String email;
	
	/* ãã¹ã¯ã¼ã? */
	private String password;

	/* å? */
	private String firstName;

	/* å§? */
	private String lastName;
	
	/* ç®¡ç?æ¨©éãã©ã° */
	private boolean administratorFlg;

	/* æ´æ°ã¦ã¼ã¶ID */
	private int updateUserId;
	
	/* æ´æ°æå» */
	private Timestamp updateAt;
	
	/* æ´æ°çªå· */
	private int updateNumber;

	/* åé¤ãã©ã° */
	private boolean delFlg;

	/**
	 * ã¦ã¼ã¶IDãåå¾ãã?
	 * @return ã¦ã¼ã¶ID
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * ã¦ã¼ã¶IDãè¨­å®ãã?
	 * @param userId ã¦ã¼ã¶ID
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * eã¡ã¼ã«ãåå¾ãã?
	 * @return eã¡ã¼ã«
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * eã¡ã¼ã«ãè¨­å®ãã?
	 * @param email eã¡ã¼ã«
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * ãã¹ã¯ã¼ããåå¾ãã?
	 * @return ãã¹ã¯ã¼ã?
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * ãã¹ã¯ã¼ããè¨­å®ãã?
	 * @param password ãã¹ã¯ã¼ã?
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * åãåå¾ãã?
	 * @return å?
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * åãè¨­å®ãã?
	 * @param firstName å?
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * å§ãåå¾ãã?
	 * @return å§?
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * å§ãè¨­å®ãã?
	 * @param lastName å§?
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * ç®¡ç?æ¨©éãã©ã°ãåå¾ãã?
	 * @return ç®¡ç?æ¨©éãã©ã°
	 */
	public boolean isAdministratorFlg() {
		return administratorFlg;
	}

	/**
	 * ç®¡ç?æ¨©éãã©ã°ãè¨­å®ãã?
	 * @param administratorFlg ç®¡ç?æ¨©éãã©ã°
	 */
	public void setAdministratorFlg(boolean administratorFlg) {
		this.administratorFlg = administratorFlg;
	}
	
	/**
	 * æ´æ°ã¦ã¼ã¶IDãåå¾ãã?
	 * @return æ´æ°ã¦ã¼ã¶ID
	 */
	public int getUpdateUserId() {
		return updateUserId;
	}

	/**
	 * æ´æ°ã¦ã¼ã¶IDãè¨­å®ãã?
	 * @param updateUserId æ´æ°ã¦ã¼ã¶ID
	 */
	public void setUpdateUserId(int updateUserId) {
		this.updateUserId = updateUserId;
	}

	/**
	 * æ´æ°æå»ãåå¾ãã?
	 * @return æ´æ°æå»
	 */
	public Timestamp getUpdateAt() {
		return updateAt;
	}

	/**
	 * æ´æ°æå»ãè¨­å®ãã?
	 * @param updateAt æ´æ°æå»
	 */
	public void setUpdateAt(Timestamp updateAt) {
		this.updateAt = updateAt;
	}

	/**
	 * æ´æ°çªå·ãåå¾ãã?
	 * @return æ´æ°çªå·
	 */
	public int getUpdateNumber() {
		return updateNumber;
	}

	/**
	 * æ´æ°çªå·ãè¨­å®ãã?
	 * @param updateNumber æ´æ°çªå·
	 */
	public void setUpdateNumber(int updateNumber) {
		this.updateNumber = updateNumber;
	}

	/**
	 * åé¤ãã©ã°ãåå¾ãã?
	 * @return åé¤ãã©ã°
	 */
	public boolean isDelFlg() {
		return delFlg;
	}

	/**
	 * åé¤ãã©ã°ãè¨­å®ãã?
	 * @param delFlg åé¤ãã©ã°
	 */
	public void setDelFlg(boolean delFlg) {
		this.delFlg = delFlg;
	}
	
}
