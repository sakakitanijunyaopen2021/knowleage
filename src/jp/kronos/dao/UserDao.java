package jp.kronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.kronos.dto.UserDto;

/**
 * ナレ�?ジ�?ーブルのDataAccessObject
 * @author Mr.X
 */
public class UserDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定す�?
	 * @param conn コネクション
	 */
    public UserDao(Connection conn) {
        this.conn = conn;
    }
    
    /**
     * ユーザ�?報を取得す�?
     * @param id ID
     * @param password パスワー�?
     * @return ユーザ�?報
     * @throws SQLException SQL例�?
     */
    public UserDto findByIdAndPassword(String id, String password) throws SQLException {

    	// SQL�?を作�?�す�?
    	StringBuffer sb = new StringBuffer();
    	sb.append(" select");
    	sb.append("        USER_ID");
    	sb.append("       ,EMAIL");
    	sb.append("       ,FIRST_NAME");
    	sb.append("       ,LAST_NAME");
    	sb.append("       ,ADMINISTRATOR_FLG");
    	sb.append("       ,UPDATE_USER_ID");
    	sb.append("       ,UPDATE_AT");
    	sb.append("       ,UPDATE_NUMBER");
    	sb.append("       ,DEL_FLG");
    	sb.append("   from USER");
    	sb.append("  where EMAIL = ?");
    	sb.append("    and PASSWORD = sha2(?, 256)");
    	sb.append("    and DEL_FLG = 0");

    	// ス�?ートメントオブジェクトを作�?�す�?
        try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホル�?ーに値をセ�?トす�?
            ps.setString(1, id);
            ps.setString(2, password);
            
            // SQLを実行す�?
            ResultSet rs = ps.executeQuery();
            
            // 結果をDTOに詰める
            if (rs.next()) {
                UserDto user = new UserDto();
                user.setUserId(rs.getInt("USER_ID"));
                user.setEmail(rs.getString("EMAIL"));
                user.setFirstName(rs.getString("FIRST_NAME"));
                user.setLastName(rs.getString("LAST_NAME"));
                user.setAdministratorFlg(rs.getBoolean("ADMINISTRATOR_FLG"));
                user.setUpdateUserId(rs.getInt("UPDATE_USER_ID"));
                user.setUpdateAt(rs.getTimestamp("UPDATE_AT"));
                user.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
                user.setDelFlg(rs.getBoolean("DEL_FLG"));
                return user;
            }
            // 該当するデータがな�?場合�?�nullを返却する
        	return null;
        }
    }
}
