package jp.kronos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.kronos.dto.ChannelDto;

/**
 * チャンネル�?ーブルのDataAccessObject
 * @author Mr.X
 */
public class ChannelDao {

	/** コネクション */
	protected Connection conn;

	/**
	 * コンストラクタ
	 * コネクションをフィールドに設定す�?
	 * @param conn コネクション
	 */
	public ChannelDao(Connection conn) {
        this.conn = conn;
    }

	/**
	 * チャンネル�?報を取得す�?
	 * @return チャンネル�?報リス�?
	 * @throws SQLException SQL例�?
	 */
	public List<ChannelDto> selectAll() throws SQLException {

		// SQL�?を作�?�す�?
		StringBuffer sb = new StringBuffer();
		sb.append("   select");
		sb.append("          CHANNEL_ID");
		sb.append("         ,CHANNEL_NAME");
		sb.append("         ,OVERVIEW");
		sb.append("         ,USER_ID");
		sb.append("         ,UPDATE_AT");
		sb.append("         ,UPDATE_NUMBER");
		sb.append("     from CHANNEL");
		sb.append(" order by CHANNEL_ID");

		List<ChannelDto> list = new ArrayList<>();
    	// ス�?ートメントオブジェクトを作�?�す�?
		try (Statement stmt = conn.createStatement()) {

			// SQL�?を実行す�?
			ResultSet rs = stmt.executeQuery(sb.toString());
			while (rs.next()) {
				ChannelDto dto = new ChannelDto();
				dto.setChannelId(rs.getInt("CHANNEL_ID"));
				dto.setChannelName(rs.getString("CHANNEL_NAME"));
				dto.setOverview(rs.getString("OVERVIEW"));
				dto.setUserId(rs.getInt("USER_ID"));
				dto.setUpdateAt(rs.getTimestamp("UPDATE_AT"));
				dto.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
				list.add(dto);
			}
			return list;
		}
	}

	/**
	 * ユーザが作�?�したチャンネルの件数を取得す�?
	 * @param userId ユーザID
	 * @return チャンネル件数
	 * @throws SQLException SQL例�?
	 */
	public int selectCountByUserId(int userId) throws SQLException {

		// SQL�?を作�?�す�?
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        count(*)");
		sb.append("   from CHANNEL");
		sb.append("  where USER_ID = ?");

    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホル�?ーに値をセ�?トす�?
			ps.setInt(1, userId);

			// SQL�?を実行す�?
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				// 該当件数を返却する
				return rs.getInt(1);
			}
		}
        // 該当するデータがな�?場合�?�0を返却する
		return 0;
	}

	/**
	 * チャンネル�?報を取得す�?
	 * @param channelId�?チャンネルID
	 * @return チャンネル�?報
	 * @throws SQLException SQL例�?
	 */
	public ChannelDto selectByChannelId(int channelId) throws SQLException {

		// SQL�?を作�?�す�?
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        CHANNEL_ID");
		sb.append("       ,CHANNEL_NAME");
		sb.append("       ,OVERVIEW");
		sb.append("       ,USER_ID");
		sb.append("       ,UPDATE_AT");
		sb.append("       ,UPDATE_NUMBER");
		sb.append("   from CHANNEL");
		sb.append("  where CHANNEL_ID = ?");

		ChannelDto dto = null;

    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホル�?ーに値をセ�?トす�?
			ps.setInt(1, channelId);

			// SQL�?を実行す�?
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				dto = new ChannelDto();
				dto.setChannelId(rs.getInt("CHANNEL_ID"));
				dto.setChannelName(rs.getString("CHANNEL_NAME"));
				dto.setOverview(rs.getString("OVERVIEW"));
				dto.setUserId(rs.getInt("USER_ID"));
				dto.setUpdateAt(rs.getTimestamp("UPDATE_AT"));
				dto.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
			}
		}
		return dto;
	}

	/**
	 * チャンネル�?報を取得す�?
	 * @param UserDto�?チャンネルID
	 * @return チャンネル�?報
	 * @throws SQLException SQL例�?
	 */
	public ChannelDto selectByChannelIdAndUserId(ChannelDto dto) throws SQLException {

		// SQL�?を作�?�す�?
		StringBuffer sb = new StringBuffer();
		sb.append(" select");
		sb.append("        CHANNEL_ID");
		sb.append("       ,CHANNEL_NAME");
		sb.append("       ,OVERVIEW");
		sb.append("       ,USER_ID");
		sb.append("       ,UPDATE_AT");
		sb.append("       ,UPDATE_NUMBER");
		sb.append("   from CHANNEL");
		sb.append("  where CHANNEL_ID = ?");
		sb.append("    and USER_ID = ?");

    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホル�?ーに値をセ�?トす�?
			ps.setInt(1, dto.getChannelId());
			ps.setInt(2, dto.getUserId());

			// SQL�?を実行す�?
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				dto = new ChannelDto();
				dto.setChannelId(rs.getInt("CHANNEL_ID"));
				dto.setChannelName(rs.getString("CHANNEL_NAME"));
				dto.setOverview(rs.getString("OVERVIEW"));
				dto.setUserId(rs.getInt("USER_ID"));
				dto.setUpdateAt(rs.getTimestamp("UPDATE_AT"));
				dto.setUpdateNumber(rs.getInt("UPDATE_NUMBER"));
			}
		}
		return dto;
	}

	/**
	 * チャンネル�?報を削除する
	 * <pre>物�?削除する</pre>
	 * @param dto チャンネル�?報
	 * @return 更新件数
	 * @throws SQLException SQL例�?
	 */
	public int delete(ChannelDto dto) throws SQLException {

    	// SQL�?を作�?�す�?
		StringBuffer sb = new StringBuffer();
		sb.append(" delete from CHANNEL");
		sb.append("       where CHANNEL_ID = ?");
		sb.append("         and UPDATE_NUMBER = ?");

    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {
        	// プレースホル�?ーに値をセ�?トす�?
			ps.setInt(1, dto.getChannelId());
			ps.setInt(2, dto.getUpdateNumber());

            // SQLを実行す�?
			return ps.executeUpdate();
		}
	}

	/**
	 * チャンネル�?報を追�?する
	 * @param dto チャンネル�?報
	 * @return 更新件数
	 * @throws SQLException SQL例�?
	 */
	public int insert(ChannelDto dto) throws SQLException {

    	// SQL�?を作�?�す�?
		StringBuffer sb = new StringBuffer();
		sb.append(" insert into CHANNEL");
		sb.append("           (");
		sb.append("             CHANNEL_NAME");
		sb.append("            ,OVERVIEW");
		sb.append("            ,USER_ID");
		sb.append("            ,UPDATE_NUMBER");
		sb.append("           )");
		sb.append("      values");
		sb.append("           (");
		sb.append("             ?");
		sb.append("            ,?");
		sb.append("            ,?");
		sb.append("            ,0");
		sb.append("           )");

    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

			// プレースホル�?ーに値をセ�?トす�?
			ps.setString(1, dto.getChannelName());
			ps.setString(2, dto.getOverview());
			ps.setInt(3, dto.getUserId());

			// SQLを実行す�?
			return ps.executeUpdate();
		}
	}

	/**
	 * チャンネル�?報を更新する
	 * @param dto チャンネル�?報
	 * @return 更新件数
	 * @throws SQLException SQL例�?
	 */
	public int update(ChannelDto dto) throws SQLException {

    	// SQL�?を作�?�す�?
		StringBuffer sb = new StringBuffer();
		sb.append(" update");
		sb.append("        CHANNEL");
		sb.append("    set");
		sb.append("        CHANNEL_NAME = ?");
		sb.append("       ,OVERVIEW = ?");
		sb.append("       ,UPDATE_NUMBER = UPDATE_NUMBER + 1");
        sb.append("       ,UPDATE_AT = CURRENT_TIMESTAMP()");
		sb.append("  where CHANNEL_ID = ?");
		sb.append("    and UPDATE_NUMBER = ?");

    	// ス�?ートメントオブジェクトを作�?�す�?
		try (PreparedStatement ps = conn.prepareStatement(sb.toString())) {

        	// プレースホル�?ーに値をセ�?トす�?
			ps.setString(1, dto.getChannelName());
			ps.setString(2, dto.getOverview());
			ps.setInt(3, dto.getChannelId());
			ps.setInt(4, dto.getUpdateNumber());

            // SQLを実行す�?
			return ps.executeUpdate();
		}
	}
}
