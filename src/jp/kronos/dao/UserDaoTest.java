package jp.kronos.dao;


import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dto.ChannelDto;
import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.KnowledgeDto;



public class UserDaoTest {

	public static void main(String[] args) throws SQLException {
	
		try (Connection conn = DataSourceManager.getConnection()) {
			// セ�?ションを取得す�?
	
			// チャンネル�?覧を取得す�?
			KnowledgeDao knowledgeDao = new KnowledgeDao(conn);
			List<KnowledgeDto> list = knowledgeDao.selectByChannelld(1);
	                           	
			for (KnowledgeDto ss : list) {
				
			System.out.println(ss);
			}
			
		} catch (SQLException | NamingException e) {
			
			// 例外メ�?セージを�?�力表示
			e.printStackTrace();
						
		}


	}

}
