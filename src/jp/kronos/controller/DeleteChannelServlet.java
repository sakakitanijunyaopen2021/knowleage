package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.BusinessLogicException;
import jp.kronos.DataSourceManager;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.ChannelDto;
import jp.kronos.dto.UserDto;


@WebServlet("/delete-channel")
public class DeleteChannelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// トップ�?��?�ジ(チャンネル�?覧)に遷移する
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// セ�?ションを取得す�?
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			// トップ�?��?�ジ(チャンネル�?覧)に遷移する
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}

		// ログインユーザ�?報を取得す�?
		UserDto user = (UserDto) session.getAttribute("user");
		
		// フォー�?の�?ータを取得す�?
		request.setCharacterEncoding("UTF-8");
		ChannelDto channelDto = new ChannelDto();
		channelDto.setChannelId(Integer.parseInt(request.getParameter("channelId")));
		channelDto.setUpdateNumber(Integer.parseInt(request.getParameter("updateNumber")));
		
		// コネクションを取得す�?
		String channelName = "";
		try (Connection conn = DataSourceManager.getConnection()) {

			try {
				// トランザクションを開始す�?
				conn.setAutoCommit(false);

				ChannelDao channelDao = new ChannelDao(conn);
				
				// ログインユーザに管�?権限がな�?、かつナレ�?ジのユーザIDと�?致しな�?場�?
				if (!user.isAdministratorFlg() && channelDao.selectByChannelId(channelDto.getChannelId()).getUserId() != user.getUserId()) {
					response.sendRedirect("access-denied.jsp");
					return;
				}
				
				// ナレ�?ジ�?報を削除する
				KnowledgeDao knowledgeDao = new KnowledgeDao(conn);
				knowledgeDao.deleteByChannelId(channelDto.getChannelId());

				channelName = channelDao.selectByChannelId(channelDto.getChannelId()).getChannelName();
				// チャンネル�?報を削除する
				int count = channelDao.delete(channelDto);
							
				if (count == 0) {
					throw new BusinessLogicException("排他制御(楽観ロック）例外");
				}
				// コミットす�?
				conn.commit();
				
				session.setAttribute("message", "「" + channelName + "」チャンネルを削除しました");
			} catch (BusinessLogicException e) {

				// ロールバックする
				conn.rollback();
				session.setAttribute("message", "「" + channelName + "」チャンネルを削除できませんでした。 排他制御");
				
			} catch (SQLException e) {
				
				// ロールバックする
				conn.rollback();
				throw e;
				
			} finally {
				conn.setAutoCommit(true);
			}

			// チャンネル�?覧画面に遷移する
			response.sendRedirect("list-channel");
			
		} catch (SQLException | NamingException e) {
			
			// 例外メ�?セージを�?�力表示
			e.printStackTrace();
			
			// シス�?�?エラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}
}
