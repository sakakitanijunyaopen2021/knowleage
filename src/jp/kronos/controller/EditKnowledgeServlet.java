package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.naming.NamingException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.FieldValidator;
import jp.kronos.DataSourceManager;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dto.ChannelDto;
import jp.kronos.dto.UserDto;

import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.KnowledgeDto;

/**
 * Servlet implementation class EditKnowledgeServlet
 */
@WebServlet("/edit-knowledge")
public class EditKnowledgeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// 　1. セッションのユーザー情報確認
		// セッション開始
		HttpSession session = request.getSession(false);
		
		if (session == null || session.getAttribute("user") == null) {
			// トップページに移動
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		// 2. セッション内のエラーメッセージの交換
			request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
			session.removeAttribute("errorMessageList");

		
		// 3. セッションスコープ内のナレッジ情報を取得する（ほとんどの場合空）
		KnowledgeDto knowledgedto = (KnowledgeDto)session.getAttribute("knowldgeDto");
		
		// 4. ナレッジ情報が存在する場合
		// 5. ナレッジ情報は存在しない場合
		if (session.getAttribute("knowledge") != null) {
			// 4 セッションのナレッジ情報の削除	
			session.removeAttribute("errorMessageList");
			
		} else {
			// 5 存在しないとき　ＩＤ取得&紐づくナレッジ情報を取得 
			KnowledgeDto knowledgeDto = new KnowledgeDto();
			
			// ナレッジID取得&セット
			String knowledgeIdStr = request.getParameter("knowledgeId");
			int knowledgeId = Integer.parseInt(knowledgeIdStr);
			//knowledgeDto.setChannelId(knowledgeId);
			
			// ナレッジ情報の取得&セット
			try(Connection conn = DataSourceManager.getConnection()) {
				// データベース接続、Daoインスタンス作成
				KnowledgeDao knowledgeDao = new KnowledgeDao(conn);
				
				//ナレッジ情報を受け取るための配列作成
				KnowledgeDto knowledgeListById = new KnowledgeDto();
				knowledgeListById = knowledgeDao.selectByKnowledgeld(knowledgeId);
				// セ�?ションからログインユーザー取得&ユーザー情報確認
				UserDto user = (UserDto)session.getAttribute("user");
				//if	(user.getUserId() != knowledgeDto.getUserId() || !user.isAdministratorFlg()) { 
				//	response.sendRedirect("access-denied.jsp");
				//	return;
				//}
				if (!user.isAdministratorFlg() && knowledgeListById.getUserId() != user.getUserId()) {
					response.sendRedirect("access-denied.jsp");
					return;
				}
				
				// 6. 取得したナレッジ情報をリクエストスコープに保持する
				request.setAttribute("knowledgeListById", knowledgeListById);
				// 7. チャンネル情報を取得し、リクエストスコープに保持する
				// ? なんでだ？
				ChannelDao channelDao = new ChannelDao(conn);
				List<ChannelDto> channelList = channelDao.selectAll();
				request.setAttribute("channelList", channelList);
				
				// 8. edit-knowledgeに転送
				request.getRequestDispatcher("WEB-INF/edit-knowledge.jsp").forward(request, response);
				//response.sendRedirect("edit-knowledge?channelId" + "=" + channelId);

			} catch (SQLException | NamingException e) {
				
				e.printStackTrace();
				response.sendRedirect("system-error.jsp");
				
			}
			
		}
		  		
	}

	
	
	
	
	
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// 1.  セッションにログイン情報が存在するか確認（index.jspに転送）
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("user") == null) {
			// トップページに移動
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		// 2. 　フォームのデータを取得（チャンネルID,ナレッジID,ナレッジ,更新番号）
		KnowledgeDto knowledgeDto = new KnowledgeDto();
			//リクエストのパラメータ取得
			request.setCharacterEncoding("UTF-8");
			knowledgeDto.setKnowledge(request.getParameter("knowledge"));
			knowledgeDto.setChannelId(Integer.parseInt(request.getParameter("channelId")));
			knowledgeDto.setUpdateNumber(Integer.parseInt(request.getParameter("updateNumber")));
			knowledgeDto.setKnowledgeId(Integer.parseInt(request.getParameter("knowledgeId")));
			knowledgeDto.setUserId(Integer.parseInt(request.getParameter("userId")));
			
		
		// 3. 　フォームの入力チェック
			//ナレッジを入力してください（エラーメッセージ）
		List<String> errorMessageList = FieldValidator.knowledgeValidation(knowledgeDto);
		// 4. 　エラーがある場合メッセージをセッションに保持し、editServletに転送
		if (errorMessageList.size() != 0) {
			// チャンネル登録画面に遷移する
			session.setAttribute("errorMessageList", errorMessageList);
			response.sendRedirect("edit-knowledge");
			return;
		}	
		
		
		// 5. 　ログインIDに権限がない場合accessdeniにリダイレクト
		UserDto user = (UserDto)session.getAttribute("user");
		//if (!user.isAdministratorFlg() && knowledgeDto.getUserId() != user.getUserId()) {
		if	(user.getUserId() != knowledgeDto.getUserId() || !user.isAdministratorFlg()) { 
			response.sendRedirect("access-denied.jsp");
			return;
		} else {
			knowledgeDto.setUserId(user.getUserId());
		}
		
		// データベースに接続
		try (Connection conn = DataSourceManager.getConnection()) {
		// 6. 　ナレッジ情報を更新する
		
			KnowledgeDao knowledgeDao = new KnowledgeDao(conn);
			knowledgeDao.updateByChannelId(knowledgeDto);
		
		
		// 7. 　メッセージを更新しました(リクエストスコープへ)
		request.setAttribute("message", "UPDATE CONPLETED");
			
		// 8. 　チャンネルIDをパラメータに保持し、list-knowledgeにリダイレクト
			//String型でよいのかなあ
		response.sendRedirect("list-knowledge?channelId=" + Integer.parseInt(request.getParameter("channelId")));
		
		} catch (SQLException | NamingException e) {
			e.printStackTrace();
			response.sendRedirect("system-error.jsp");	
		}
	
	}

}
