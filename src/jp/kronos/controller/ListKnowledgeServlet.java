package jp.kronos.controller;

import java.io.IOException;
import java.util.List;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.naming.NamingException;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dto.ChannelDto;
import jp.kronos.dto.KnowledgeDto;
import jp.kronos.dto.UserDto;
import jp.kronos.dao.KnowledgeDao;

/**
 * Servlet implementation class ListKnowledgeServlet
 */
@WebServlet("/list-knowledge")
public class ListKnowledgeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try (Connection conn = DataSourceManager.getConnection()) {
			
			// ここfalseの気がするなぜなら、管理者と管理者じゃない人がいて、管理者じゃない人の情報は
			// 	セッションに入れておくがないから
			HttpSession session = request.getSession(true);
			
			UserDto user = (UserDto)session.getAttribute("user");
			
			// チャンネルIDを取得する
			String ChannelIdStr = request.getParameter("channelId");
			// ここあってるか不安ポイント1
			// int型に成形
			int ChannelId = Integer.parseInt(ChannelIdStr);	
			
			KnowledgeDao dao = new KnowledgeDao(conn);
			
			List<KnowledgeDto> knowledgeList = dao.selectByChannelld(ChannelId);
			
			// ナレッジ情報リストをリクエストスコープに保持
			request.setAttribute("knowledgeList", knowledgeList);
			
			// URIをリクエストに保持する
			request.setAttribute("uri", request.getRequestURI() + "?channelId" + "=" + ChannelId);
			
			// ナレッジ情報リストをリクエストスコープに保持
			request.setAttribute("channelId", ChannelId);
						
			
			// メ�?セージをリクエストに保持する
			request.setAttribute("message", session.getAttribute("message"));
			session.removeAttribute("message");
			
			// ナブバ�?�メ�?セージをリクエストに保持する
			request.setAttribute("navbarMessage", session.getAttribute("navbarMessage"));
			session.removeAttribute("navbarMessage");

			// チャンネル�?覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/list-knowledge.jsp").forward(request, response);
			
			
		} catch (SQLException | NamingException e) {
			
			// 例外メ�?セージを�?�力表示
			e.printStackTrace();
			// シス�?�?エラー画面に遷移する
			response.sendRedirect("system-error.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
