package jp.kronos.controller;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.FieldValidator;
import jp.kronos.DataSourceManager;
import jp.kronos.dao.ChannelDao;
import jp.kronos.dto.ChannelDto;
import jp.kronos.dto.UserDto;

import jp.kronos.dao.KnowledgeDao;
import jp.kronos.dto.KnowledgeDto;

/**
 * Servlet implementation class AddKnowledgeServlet
 */
@WebServlet("/add-knowledge")
public class AddKnowledgeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// ログインユーザーの照合
		// セッション開始
		HttpSession session = request.getSession(false);
		
		if (session == null || session.getAttribute("user") == null) {
			// トップページに移動
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		
		try (Connection conn = DataSourceManager.getConnection()) {
			// チャンネルを追�?する
			
			// List型　addListを作成
			List<ChannelDto> addList = new ArrayList();
			ChannelDao channelDao = new ChannelDao(conn);
			List<ChannelDto> channelList = channelDao.selectAll();
			
			// チャンネル一覧データをリクエストに保持する
			request.setAttribute("channelList", channelList);
			
			// エラーメ�?セージをリクエストに保持する
			request.setAttribute("errorMessageList", session.getAttribute("errorMessageList"));
			session.removeAttribute("errorMessageList");
			
			// チャンネル�?覧画面に遷移する
			request.getRequestDispatcher("WEB-INF/add-knowledge.jsp").forward(request, response);
			
		} catch (SQLException | NamingException e) {
						
			
				e.printStackTrace();
				
				// シス�?�?エラー画面に遷移する
				response.sendRedirect("system-error.jsp");
		}
		
	

	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		// ログインユーザーの照合
		// セッション開始
		HttpSession session = request.getSession(false);
		
		if (session == null || session.getAttribute("user") == null) {
			// トップページに移動
			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		}
		
		// セ�?ションからログインユーザ�?報を取得す�?
		UserDto user = (UserDto)session.getAttribute("user");
		
		request.setCharacterEncoding("UTF-8");
		// channelIdをint型に
		String ChannelIdStr = request.getParameter("channelId");
		int channelId = Integer.parseInt(ChannelIdStr);
		
		// knowledgeをString型に形成 意味ない
		String knowledge = request.getParameter("knowledge");
		
		// フォー�?の�?ータを取得す�?
		request.setCharacterEncoding("UTF-8");
		KnowledgeDto knowledgeDto = new KnowledgeDto();
		knowledgeDto.setChannelId(channelId);
		knowledgeDto.setKnowledge(knowledge);
		knowledgeDto.setUserId(user.getUserId());
		
		
		// 入力チェ�?ク
		List<String> errorMessageList = FieldValidator.knowledgeValidation(knowledgeDto);
		if (errorMessageList.size() != 0) {
			// チャンネル登録画面に遷移する
			session.setAttribute("errorMessageList", errorMessageList);
			response.sendRedirect("add-knowledge");
			return;
		}
		
		// データベースに接続
		try (Connection conn = DataSourceManager.getConnection()) {
			// チャンネルを追�?する
			
			KnowledgeDao dao = new KnowledgeDao(conn);
			dao.insert(knowledgeDto);
			
			// チャンネル名をリクエストスコープに保持する
			// !!セッションだったから変えたよ？！　テストして！
			request.setAttribute("message", knowledge + "を登録しました");
			
			// チャンネル�?覧画面に遷移する
			response.sendRedirect("list-knowledge?channelId=" + request.getParameter("channelId"));
		
		} catch (SQLException | NamingException e) {
			
				e.printStackTrace();
				response.sendRedirect("system-error.jsp");
			
		}
	}

}
