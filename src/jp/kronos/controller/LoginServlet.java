package jp.kronos.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.kronos.DataSourceManager;
import jp.kronos.dao.UserDao;
import jp.kronos.dto.UserDto;


@WebServlet(urlPatterns={"/login"}, initParams={@WebInitParam(name="password", value="knowledge123")})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// トップ�?��?�ジ(チャンネル�?覧)に遷移する
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// フォー�?の�?ータを取得す�?
		String loginId = request.getParameter("id");
		String loginPassword = request.getParameter("password");
		String uri = request.getParameter("uri");

		// セ�?ションを取得す�?
		HttpSession session = request.getSession(true);

		// ログインID、パスワードが未入力�?�場�?
		if ("".equals(loginId) || "".equals(loginPassword)) {

			session.setAttribute("navbarMessage", "メールアドレス、パスワードを入力してください");
			
			// ログイン処�?前にペ�?�ジ�?報が存在しな�?場合�?�チャンネル�?覧に遷移する
			response.sendRedirect(uri);
			return;
		}
		
		try (Connection conn = DataSourceManager.getConnection()) {

			// ログイン処�?
			UserDao loginDao = new UserDao(conn);
			UserDto userDto = loginDao.findByIdAndPassword(loginId, loginPassword);

			session.setAttribute("user", userDto);
			session.removeAttribute("navbarMessage");

			// ログイン失敗時
			if (userDto == null) {
				session.setAttribute("navbarMessage", "メールアドレスまた�?�パスワードが間違って�?ま�?");
			}

			// 初回ログイン�?
			if (getInitParameter("password").equals(loginPassword)) {
				session.setAttribute("isChangeRequired", true);
				response.sendRedirect("change-password");
				return;
			}
			
			// ログイン処�?前にペ�?�ジ�?報が存在しな�?場合�?�チャンネル�?覧に遷移する
			response.sendRedirect(uri);
			
		} catch (SQLException | NamingException e) {
			// 例外メ�?セージを�?�力表示
			e.printStackTrace();
			
			// シス�?�?エラーに遷移する
			response.sendRedirect("system-error.jsp");
		}
	}
}
