<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>チャンネル登録</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>

	<div class="col-12">
		<h4 class="title">ナレッジ追加</h4>
		<br>
		<c:forEach items="${ errorMessageList }" var="errorMessage">
			<div class="error-message">
				<c:out value="${ errorMessage }" />
			</div>
		</c:forEach>
		<br>
		<form action="add-knowledge" method="post">	
			<div class="form-group row">
				<label class="col-form-label col-2 head">チャンネル</label>
				<div class="col-8">
					<select name="channelId" >
						<c:forEach items="${ channelList }" var="channel">
						<option value="${ channel.channelId }">${ channel.channelName }</option>
						</c:forEach>
					</select>
				</div>
			<div class="form-group row">
				<label class="col-form-label col-2 head">ナレッジ</label>
				<div class="col-8">
					<textarea name="knowledge" style="resize: none;" class="form-control" maxlength="66535" ><c:out value="${ KnowledgeDto.knowledge }" /></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="mx-auto">
					<!-- for文 -->
					<button type="submit" class="btn btn-primary">INSERT</button>
				</div>
			</div>
		</form>
	</div>


</body>
</html>