<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ナレッジ一覧</title>
<script type="text/javascript">
<!--
	function deleteConfirm(channelName) {
		return window.confirm("「" + channelName + "」チャンネルを本当に削除しますか？");
	}
// -->
</script>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="col-12">
		<h4 class="title">ナレッジ一覧</h4>
		<!-- messageわからん --> 
		<div class="message">
			<c:out value="${ message }" />
		</div>
		<form action="search-knowledge" method="get">
					<input type="text" name="keyword"　class="form-control" value="${ sessionScope.keyword }" autofocus>
					<input type="hidden" name="uri" value="${ requestScope.uri }">
					<button type="submit" class="btn btn-warning btn-sm">検索</button>
					<a href="add-knowledge">ナレッジ追加</a>
		</form>		
		<c:choose>
			<c:when  test="${ not empty knowledgeList }">
				<table class="table table-striped">
					<tr>
						<th>チャンネル名</th>
						<th>ナレッジ</th>
						<th>最終更新日時</th>
					</tr>
					<c:forEach items="${ knowledgeList }" var="knowledgeDto">
						<tr>
							<!-- <td><c:out value="${ knowledgeDto.channelName }" /></td> -->
							<td><c:out value="${ knowledgeDto.channelName }" /></td>
							<td><c:out value="${ knowledgeDto.knowledge }" /></td>
							<c:choose>
								<c:when test="${ user.userId == knowledgeDto.userId || user.administratorFlg }">
							    	<td><a href="edit-knowledge?knowledgeId=${ knowledgeDto.knowledgeId }&channelId=${ requestScope.channelId}"><c:out value="${ knowledgeDto.updateAt }" /></a></td>
								</c:when>
								<c:otherwise>
							   		<td><c:out value="${ knowledgeDto.updateAt }" /></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach>
				</table>
			</c:when>
			<c:otherwise>
						<h1><c:out value="検索キーワードに一致する検索結果はありません" /><h1>
			</c:otherwise>	
		</c:choose>
	</div>
</body>
</html>