<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ナレッジ編集</title>
<%@ include file="header.jsp"%>
</head>
<body>
	<%@ include file="navbar.jsp"%>
	<div class="col-12">
		<h4 class="title">ナレッジ編集</h4>
		<br>
		<c:forEach items="${ errorMessageList }" var="errorMessage">
			<div class="error-message">
				<c:out value="${ errorMessage }" />
			</div>
		</c:forEach>
		<br>
		<form action="edit-knowledge" method="post">	
		<input type="hidden" name="knowledgeId" value="${ knowledgeListById.knowledgeId }" />
		<input type="hidden" name="updateNumber" value="${ knowledgeListById.updateNumber }" />
		<input type="hidden" name="userId" value="${ knowledgeListById.userId }" />
			<div class="form-group row">
				<label class="col-form-label col-2 head">ナレッジ</label>
				<div class="col-8">
					<select name="channelId" >
						<c:forEach items="${ channelList }" var="channel">
						<option value="${ channel.channelId }">${ channel.channelName }</option>
						<a href="list-knowledge?channelId=${ channel.channelId }">BACK</a> 
						</c:forEach>
					</select>
				</div>
			<div class="form-group row">
				<label class="col-form-label col-2 head">ナレッジ</label>
				<div class="col-8">
					<textarea name="knowledge" style="resize: none;" class="form-control" maxlength="66535" ><c:out value="${ knowledgeListById.knowledge }" /></textarea>
				</div>
			</div>
			<div class="form-group row">
				<div class="mx-auto">
					<br>
					<br>
					<br>
					<br>
					<button type="reset" class="btn btn-secondaqry">BACK</button>
					<button type="submit" class="btn btn-primary">UPDATE</button>
					<button formaction="delete-knowledge" type="submit" class="btn btn-Danger">DELETE</button>
				
				</div>
			</div>
		</form>
	</div>


</body>
</html>